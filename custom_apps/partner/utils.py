import requests
import sys

from django.core.cache import cache
from xml.etree import ElementTree
from django.conf import settings


def get_curr():
    # CurrenciesLayer
    currencieslayerapi = requests.get(settings.CURRENCIE_URLS['currlayer'])
    if currencieslayerapi.status_code == 200:
        curr_cont = currencieslayerapi.json()
        if curr_cont['success']:
            return curr_cont['quotes']['USDRUB']

    # European CB
    ecbapi = requests.get(settings.CURRENCIE_URLS['ecb'])
    if currencieslayerapi.status_code == 200:
        ecb_cont = ElementTree.fromstring(ecbapi.content)
        rub_curr = None
        usd_curr = None
        for item in ecb_cont[2][0]:
            items_in = item.items()
            if items_in[0][1] == "RUB":
                rub_curr = items_in[1][1]
            elif items_in[0][1] == "USD":
                usd_curr = items_in[1][1]

        if rub_curr and usd_curr:
            return float(rub_curr) / float(usd_curr)

    # Shutdown server if no respones of currencies
    sys.exit(0)


def rubtousd():
    currencie_current = cache.get('curr_rub')
    if not currencie_current:
        curr_now = get_curr()
        cache.set('curr_rub', curr_now, settings.CURRENCIE_TIMEOUT + 100)
        return curr_now
    else:
        return currencie_current

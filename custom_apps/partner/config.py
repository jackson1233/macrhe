from oscar.apps.partner import config


class PartnerConfig(config.PartnerConfig):
    name = 'custom_apps.partner'

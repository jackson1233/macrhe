from oscar.apps.partner.availability import Base


class StockRequired(Base):
    """
    Allow a product to be bought while there is stock.  This policy is
    instantiated with a stock number (``num_available``).  It ensures that the
    product is only available to buy while there is stock available.

    This is suitable for physical products where back orders (eg allowing
    purchases when there isn't stock available) are not permitted.
    """
    CODE_IN_STOCK = 'instock'
    CODE_OUT_OF_STOCK = 'outofstock'

    def __init__(self, num_available):
        self.num_available = num_available

    def is_purchase_permitted(self, quantity):
        if self.num_available <= 0:
            return False, "Нет в наличии"
        if quantity > self.num_available:
            msg = "Доступно на складе %(max)d шт." % {
                'max': self.num_available}
            return False, msg
        return True, ""

    @property
    def code(self):
        if self.num_available > 0:
            return self.CODE_IN_STOCK
        return self.CODE_OUT_OF_STOCK

    @property
    def short_message(self):
        if self.num_available > 0:
            return "Доступно"
        return "Не доступно"

    @property
    def message(self):
        if self.num_available > 0:
            return "Доступно (%d шт.)" % self.num_available
        return "Не доступно"
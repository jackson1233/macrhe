from django.conf import settings

from decimal import Decimal as D

from custom_apps.partner.utils import rubtousd

from oscar.apps.shipping import methods
from oscar.core import prices


class DeliveryIn(methods.Base):
    code = 'deliveryMO'
    name = 'Доставка по Москве'

    def calculate(self, basket):
        excl_tax = settings.DELIVERY_MOSCOW_PRICE_USD
        incl_tax = float(excl_tax * rubtousd())
        return prices.Price(
            currency=basket.currency,
            excl_tax=D(excl_tax), incl_tax=D(incl_tax))


class DeliveryOut(methods.Base):
    code = 'deliveryRU'
    name = 'Доставка по России'

    def calculate(self, basket):
        excl_tax = settings.DELIVERY_RUSSIA_PRICE_USD
        incl_tax = float(excl_tax * rubtousd())
        return prices.Price(
            currency=basket.currency,
            excl_tax=D(excl_tax), incl_tax=D(incl_tax))

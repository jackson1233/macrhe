from oscar.apps.shipping import repository
from oscar.core.loading import get_model
from . import methods


class Repository(repository.Repository):
    def get_available_shipping_methods(self, basket, user=None,
            shipping_addr=None, request=None, **kwargs):
        method = (methods.DeliveryIn(),)
        if shipping_addr:
            method = (methods.DeliveryIn(), methods.DeliveryOut())

        # If no address was specified, or weight-based options are
        # not available, return the "Reserve" shipping option
        return method

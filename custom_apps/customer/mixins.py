import logging

from django.conf import settings
from django.contrib.auth import login as auth_login
from django.contrib.auth import authenticate
from django.contrib.sites.shortcuts import get_current_site

from oscar.apps.customer.signals import user_registered
from oscar.core.compat import get_user_model
from oscar.core.loading import get_model, get_class

from custom_set.models import VerificationCode


User = get_user_model()
CommunicationEventType = get_model('customer', 'CommunicationEventType')
Dispatcher = get_class('customer.utils', 'Dispatcher')

logger = logging.getLogger('oscar.customer')


class RegisterUserMixin(object):
    communication_type_code = 'REGISTRATION'

    def register_user(self, form):
        """
        Create a user instance and send a new registration email (if configured
        to).
        """
        user = form.save(commit=False)
        user.is_active = False
        user = form.save()
        ver_code = VerificationCode(user=user)
        ver_code.save()

        # Raise signal robustly (we don't want exceptions to crash the request
        # handling).
        user_registered.send_robust(
            sender=self, request=self.request, user=user)

        if getattr(settings, 'OSCAR_SEND_REGISTRATION_EMAIL', True):
            self.send_registration_email(user, ver_code)

        return user

    def send_registration_email(self, user, ver_code):
        code = self.communication_type_code
        ctx = {'user': user,
               'site': get_current_site(self.request),
               'ver_url': ver_code.get_absolute_url()}
        messages = CommunicationEventType.objects.get_and_render(
            code, ctx)
        if messages and messages['body']:
            Dispatcher().dispatch_user_messages(user, messages)

from datetime import datetime

from django import http
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import login as auth_login
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect, get_object_or_404
from django.views import generic

from oscar.apps.customer.utils import get_password_reset_url
from oscar.core.loading import get_classes, get_class, get_model
from oscar.apps.customer import signals

from custom_set.models import VerificationCode


RegisterUserMixin, PageTitleMixin = get_classes('customer.mixins', ['RegisterUserMixin', 'PageTitleMixin'])
EmailAuthenticationForm, EmailUserCreationForm = get_classes('customer.forms', ['EmailAuthenticationForm', 'EmailUserCreationForm'])
ProfileForm = get_class('customer.forms', 'ProfileForm')
Dispatcher = get_class('customer.utils', 'Dispatcher')
CommunicationEventType = get_model('customer', 'CommunicationEventType')
SetPasswordForm = get_class('customer.forms', 'SetPasswordForm')


def confirm_email(request, ver_code):
    code = get_object_or_404(VerificationCode, code=str(ver_code).replace('-', ''))
    now_dt = datetime.now()

    # Check if user is not already active
    if code.user.is_active:
        raise http.Http404()

    # Check if verification time is not over
    diff_dt = (now_dt - code.date_created).total_seconds() // 60 // 60 // 24
    if diff_dt > 7:
        code.user.remove()
        raise http.Http404()

    # Verify user
    code.user.is_active = True
    code.user.save()
    # Grab a reference to the session ID before logging in
    old_session_key = request.session.session_key

    auth_login(request, code.user, backend='oscar.apps.customer.auth_backends.EmailBackend')

    # Raise signal robustly (we don't want exceptions to crash the
    # request handling). We use a custom signal as we want to track the
    # session key before calling login (which cycles the session ID).
    signals.user_logged_in.send_robust(
        sender=confirm_email, request=request, user=code.user,
        old_session_key=old_session_key)

    code.delete()
    return redirect(settings.LOGIN_REDIRECT_URL)


class AccountAuthView(RegisterUserMixin, generic.RedirectView):
    """
    This is actually a slightly odd double form view that allows a customer to
    either login or register.
    """
    login_prefix, registration_prefix = 'login', 'registration'
    login_form_class = EmailAuthenticationForm
    registration_form_class = EmailUserCreationForm
    redirect_field_name = 'next'

    def get(self, request, *args, **kwargs):
        return redirect(reverse('basket:summary') + '?errors=log')

    def post(self, request, *args, **kwargs):
        # Use the name of the submit button to determine which form to validate
        if u'login_submit' in request.POST:
            return self.validate_login_form()
        elif u'registration_submit' in request.POST:
            return self.validate_registration_form()
        return http.HttpResponseBadRequest()

    def get_login_form(self, bind_data=False):
        return self.login_form_class(
            **self.get_login_form_kwargs(bind_data))

    def get_login_form_kwargs(self, bind_data=False):
        kwargs = {}
        kwargs['host'] = self.request.get_host()
        kwargs['prefix'] = self.login_prefix
        kwargs['initial'] = {
            'redirect_url': self.request.GET.get(self.redirect_field_name, ''),
        }
        if bind_data and self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def validate_login_form(self):
        form = self.get_login_form(bind_data=True)
        if form.is_valid():
            user = form.get_user()

            # Grab a reference to the session ID before logging in
            old_session_key = self.request.session.session_key

            auth_login(self.request, form.get_user())

            # Raise signal robustly (we don't want exceptions to crash the
            # request handling). We use a custom signal as we want to track the
            # session key before calling login (which cycles the session ID).
            signals.user_logged_in.send_robust(
                sender=self, request=self.request, user=user,
                old_session_key=old_session_key)

            return redirect(self.get_login_success_url(form))

        self.request.session['login_form'] = form.errors
        return redirect(reverse('promotions:home') + '?errors=log')

    def get_login_success_url(self, form):
        redirect_url = form.cleaned_data['redirect_url']
        if redirect_url:
            return redirect_url

        # Redirect staff members to dashboard as that's the most likely place
        # they'll want to visit if they're logging in.
        if self.request.user.is_staff:
            return reverse('dashboard:index')

        return settings.LOGIN_REDIRECT_URL

    # REGISTRATION

    def get_registration_form(self, bind_data=False):
        return self.registration_form_class(
            **self.get_registration_form_kwargs(bind_data))

    def get_registration_form_kwargs(self, bind_data=False):
        kwargs = {}
        kwargs['host'] = self.request.get_host()
        kwargs['prefix'] = self.registration_prefix
        kwargs['initial'] = {
            'redirect_url': self.request.GET.get(self.redirect_field_name, ''),
        }
        if bind_data and self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def validate_registration_form(self):
        form = self.get_registration_form(bind_data=True)
        if form.is_valid():
            self.register_user(form)

            return redirect(self.get_registration_success_url(form))

        self.request.session['registr_form'] = form.errors
        return redirect(reverse('promotions:home') + '?errors=reg')

    def get_registration_success_url(self, form):
        messages.success(self.request, "Регистрация почти завершена. Подтвердите вашу почту через ссылку, которую мы вам отправили. Не забудьте проверить папку спам")
        return reverse('promotions:home') + '?reg=success'


class ProfileUpdateView(PageTitleMixin, generic.FormView):
    form_class = ProfileForm
    template_name = 'customer/profile/profile_form.html'
    communication_type_code = 'EMAIL_CHANGED'
    page_title = 'Изменить профиль'
    active_tab = 'profile'
    success_url = reverse_lazy('customer:profile-view')

    def get_form_kwargs(self):
        kwargs = super(ProfileUpdateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        # Grab current user instance before we save form.  We may need this to
        # send a warning email if the email address is changed.
        form.save()

        return redirect(self.get_success_url())


class SetPasswordView(PageTitleMixin, generic.FormView):
    form_class = SetPasswordForm
    template_name = 'customer/profile/change_password_form.html'
    communication_type_code = 'REGISTRATION'
    page_title = 'Установить пароль'
    active_tab = 'profile'
    success_url = reverse_lazy('customer:profile-view')

    def get_form_kwargs(self):
        kwargs = super(SetPasswordView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, self.request.user)
        messages.success(self.request, "Пароль установлен")

        ctx = {
            'user': self.request.user,
            'site': get_current_site(self.request),
            'reset_url': get_password_reset_url(self.request.user),
        }
        msgs = CommunicationEventType.objects.get_and_render(
            code=self.communication_type_code, context=ctx)
        Dispatcher().dispatch_user_messages(self.request.user, msgs)

        return redirect(self.get_success_url())

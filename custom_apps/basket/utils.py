from oscar.apps.basket.utils import BasketMessageGenerator as bmg


def get_attributes_of_prod(product):
    res = ''
    for attribute in product.attribute_values.all().prefetch_related('attribute')[:2]:
        attr_val = str(attribute.value_as_html)
        res += attr_val + ' '
    return res.lower()


class BasketMessageGenerator(bmg):
    def get_messages(self, basket, offers_before, offers_after, include_buttons=True):
        messages = []
        messages.extend(self.get_offer_messages(offers_before, offers_after))
        return messages

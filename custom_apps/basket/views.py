import json

from oscar.apps.basket.views import BasketAddView as baw, BasketView as bw
from django.http import HttpResponse
from django.template.loader import render_to_string


class BasketView(bw):
    def json_response(self, ctx, flash_messages):
        ctx.setdefault('noshipping', True)
        basket_html = render_to_string(
            'basket/partials/basket_totals.html',
            context=ctx, request=self.request)

        payload = {
            'content_html': basket_html,
            'messages': flash_messages.as_dict()}
        return HttpResponse(json.dumps(payload),
                            content_type="application/json")


class BasketAddView(baw):
    def get_success_url(self):
        return self.product.get_absolute_url()

from django.utils.translation import ugettext_lazy as _
from oscar.apps.basket.abstract_models import AbstractLine
from oscar.core.loading import get_class
from decimal import Decimal as D

from custom_apps.partner.utils import rubtousd
from custom_apps.basket.templatetags.currency_filters import currency_custom as currency

Unavailable = get_class('partner.availability', 'Unavailable')


class Line(AbstractLine):

    def get_warning(self):
        """
        Return a warning message about this basket line if one is applicable

        This could be things like the price has changed
        """
        if isinstance(self.purchase_info.availability, Unavailable):
            msg = u"'%(product)s' is no longer available"
            return _(msg) % {'product': self.product.get_title()}

        if not self.price_incl_tax:
            return
        if not self.purchase_info.price.is_tax_known:
            return

        # Compare current price to price when added to basket
        current_price_incl_tax = self.purchase_info.price.incl_tax
        if current_price_incl_tax != self.price_incl_tax:
            product_prices = {
                'product': self.product.get_title(),
                'old_price': currency(self.price_incl_tax),
                'new_price': currency(current_price_incl_tax)
            }
            if current_price_incl_tax > self.price_incl_tax:
                warning = _("The price of '%(product)s' has increased from"
                            " %(old_price)s to %(new_price)s since you added"
                            " it to your basket")
                return warning % product_prices
            else:
                warning = _("The price of '%(product)s' has decreased from"
                            " %(old_price)s to %(new_price)s since you added"
                            " it to your basket")
                return warning % product_prices

    @property
    def line_price_excl_tax_incl_discounts(self):
        return self.line_price_excl_tax - (self._discount_excl_tax / D(rubtousd()))

from oscar.apps.basket.models import *  # noqa isort:skip

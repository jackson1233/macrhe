import phonenumbers

from django import forms
from django.conf import settings
from django.core import validators
from django.utils.translation import ugettext_lazy as _
from oscar.core.loading import get_model
from oscar.forms.mixins import PhoneNumberMixin
from phonenumber_field.phonenumber import PhoneNumber

UserAddress = get_model('address', 'useraddress')
country = get_model('address', 'country')


class AbstractAddressForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        """
        Set fields in OSCAR_REQUIRED_ADDRESS_FIELDS as required.
        """
        super(AbstractAddressForm, self).__init__(*args, **kwargs)
        field_names = settings.OSCAR_REQUIRED_ADDRESS_FIELDS
        for field_name in field_names:
            if field_name in self.fields:
                self.fields[field_name].required = True


class UserAddressForm(PhoneNumberMixin, AbstractAddressForm):

    class Meta:
        model = UserAddress
        fields = [
            'first_name', 'last_name', 'phone_number',
            'line4', 'line1', 'line2', 'line3',
            'postcode', 'notes',
        ]
        labels = {
            'line1': 'Улица',
            'line2': 'Дом',
            'line3': 'Квартира'
        }

    def __init__(self, user, *args, **kwargs):
        super(UserAddressForm, self).__init__(*args, **kwargs)
        self.instance.user = user
        self.instance.country = country.objects.first()

    def get_country(self):
        # If the form data contains valid country information, we use that.
        return 'RU'

    def set_country_and_region_code(self):
        # Try hinting with the shipping country if we can determine one.
        self.country = self.instance.country

        if self.country:
            self.region_code = self.country.iso_3166_1_a2

    def clean_phone_number_field(self, field_name):
        number = self.cleaned_data.get(field_name)

        # Empty
        if number in validators.EMPTY_VALUES:
            return ''

        # Check for an international phone format
        try:
            phone_number = PhoneNumber.from_string(number)
        except phonenumbers.NumberParseException:

            if not self.region_code:
                # There is no shipping country, not a valid international number
                self.add_error(
                    field_name,
                    _(u'This is not a valid international phone format.'))
                return number

            # The PhoneNumber class does not allow specifying
            # the region. So we drop down to the underlying phonenumbers
            # library, which luckily allows parsing into a PhoneNumber
            # instance.
            try:
                phone_number = PhoneNumber.from_string(number,
                                                       region=self.region_code)
                if not phone_number.is_valid():
                    self.add_error(
                        field_name,
                        _(u'This is not a valid local phone format for %s.')
                        % self.country)
            except phonenumbers.NumberParseException:
                # Not a valid local or international phone number
                self.add_error(
                    field_name,
                    _(u'This is not a valid local or international phone format.'))
                return number

        return phone_number

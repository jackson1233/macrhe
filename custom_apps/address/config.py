from oscar.apps.address import config


class AddressConfig(config.AddressConfig):
    name = 'custom_apps.address'

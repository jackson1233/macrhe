from oscar.apps.catalogue import config


class CatalogueConfig(config.CatalogueConfig):
    name = 'custom_apps.catalogue'

    def ready(self):
        from . import receivers

from django.conf import settings

from haystack.query import SQ
from haystack import connections

from oscar.templatetags.category_tags import get_annotated_list
from oscar.core.loading import get_class, get_model


BrowseCategoryForm = get_class('search.forms', 'BrowseCategoryForm')
SearchHandler = get_class('search.search_handlers', 'SearchHandler')
Product = get_model('catalogue', 'Product')


def get_search_attrs(sqs, get_params):
    attrs_used = {}
    attr_list = []
    for item in sqs:
        # is that item with exact params in exact category
        if item.attrs:
            for attr in item.attrs:
                attr_code, attr_lbl, attr_val = attr.split(': ')
                if attr_code != 'pol':
                    if attr_code not in attrs_used:
                        checked = []
                        for attr_param in get_params.getlist(attr_code):
                            checked.append(attr_param.split(': ')[-1])
                        prev_attr = {'label': attr_lbl,
                                     'code': attr_code,
                                     'values': [attr_val],
                                     'checked': checked}
                        attrs_used[attr_code] = prev_attr
                        attr_list.append(prev_attr)
                    else:
                        list_attr = attrs_used[attr_code]['values']
                        if attr_val not in list_attr:
                            list_attr.append(attr_val)
    return attr_list


def get_gender_cats(sqs):
    cat_tree = get_annotated_list()
    for item in sqs:
        cats_item_used = []
        if item.category:
            for category in item.category:
                cats = category.split(' > ')
                for cat in cats:
                    if cat not in cats_item_used:
                        cats_item_used.append(cat)
                        for node in cat_tree:
                            if node[0].name == cat:
                                if item.structure != 'child':
                                    if node[1].get('count'):
                                        node[1]['count'] += 1
                                    else:
                                        node[1]['count'] = 1
    return cat_tree


class SolrProductSearchHandler(SearchHandler):
    """
    Search handler specialised for searching products.  Comes with optional
    category filtering. To be used with a Solr search backend.
    """
    form_class = BrowseCategoryForm
    model_whitelist = [Product]
    paginate_by = settings.OSCAR_PRODUCTS_PER_PAGE

    def __init__(self, request_data, full_path, categories=None):
        self.categories = categories
        self.attr_search = None
        self.tree_categories = None
        super(SolrProductSearchHandler, self).__init__(request_data, full_path)

    def set_cats_and_attrs(self, query, get_params):
        is_gender_based = self.tree_categories
        cat_tree = self.tree_categories if is_gender_based else get_annotated_list()
        attr_list = []
        attrs_used = {}
        is_search = self.attr_search
        filter_count = len(get_params) or is_search

        for item in query:
            # is that item with exact params in exact category
            if item.attrs and not is_search:
                for attr in item.attrs:
                    attr_code, attr_lbl, attr_val = attr.split(': ')
                    if attr_code != 'pol':
                        if attr_code not in attrs_used:
                            checked = []
                            for attr_param in get_params.getlist(attr_code):
                                checked.append(attr_param.split(': ')[-1])
                            prev_attr = {'label': attr_lbl,
                                         'code': attr_code,
                                         'values': [attr_val],
                                         'checked': checked}
                            attrs_used[attr_code] = prev_attr
                            attr_list.append(prev_attr)
                        else:
                            list_attr = attrs_used[attr_code]['values']
                            if attr_val not in list_attr:
                                list_attr.append(attr_val)

            # get category tree and product counts for exact category
            if item.category and filter_count and not is_gender_based:
                cats_item_used = []
                for category in item.category:
                    cats = category.split(' > ')
                    for cat in cats:
                        if cat not in cats_item_used:
                            cats_item_used.append(cat)
                            for node in cat_tree:
                                if node[0].name == cat:
                                    if item.structure != 'child':
                                        if node[1].get('count'):
                                            node[1]['count'] += 1
                                        else:
                                            node[1]['count'] = 1

        if not filter_count and not is_gender_based:
            for cat in cat_tree:
                prod_count = 0
                cats_list = cat[0].get_descendants_and_self()
                prod_count = Product.objects.filter(categories__in=cats_list).exclude(structure='child').count()
                if prod_count > 0:
                    cat[1]['count'] = prod_count

        cat_tree_res = []
        for cat in cat_tree:
                if cat[1].get('count'):
                    cat_tree_res.append(cat)

        self.tree_categories = cat_tree_res
        if not is_search:
            self.attr_search = attr_list
            self.attr_search.sort(key=lambda val: val['label'])

    # BULKING =================================================
    def bulk_fetch_results(self, paginated_results):
        """
        This method gets paginated search results and returns a list of Django
        objects in the same order.

        It preserves the order without doing any ordering in Python, even
        when more than one Django model are returned in the search results. It
        also uses the same queryset that was used to populate the search
        queryset, so any select_related/prefetch_related optimisations are
        in effect.

        It is heavily based on Haystack's SearchQuerySet.post_process_results,
        but works on the paginated results instead of all of them.
        """
        objects = []

        models_pks = loaded_objects = {}
        for result in paginated_results:
            models_pks.setdefault(result.model, []).append(result.pk)

        search_backend_alias = self.results.query.backend.connection_alias
        for model in models_pks:
            ui = connections[search_backend_alias].get_unified_index()
            index = ui.get_index(model)
            queryset = index.read_queryset(using=search_backend_alias).select_related('product_class').prefetch_related('stockrecords', 'images', 'categories')
            loaded_objects[model] = queryset.in_bulk(models_pks[model])

        for result in paginated_results:
            model_objects = loaded_objects.get(result.model, {})
            try:
                result._object = model_objects[int(result.pk)]
            except KeyError:
                # The object was either deleted since we indexed or should
                # be ignored; fail silently.
                pass
            else:
                objects.append(result._object)

        return objects
    # =========================================================

    def get_search_queryset(self):
        sqs = super(SolrProductSearchHandler, self).get_search_queryset()

        request_data = self.request_data.copy()
        # Doesn't it exclude pagination ?
        if self.categories:
            # We use 'narrow' API to ensure Solr's 'fq' filtering is used as
            # opposed to filtering using 'q'.
            self.categories += list(self.categories[0].get_children())
            pattern = ' OR '.join([
                '"%s"' % c.full_name for c in self.categories])
            sqs = sqs.narrow('category_exact:(%s)' % pattern)

        q = None
        if 'q' in request_data:
            q = request_data.pop('q')
            self.attr_search = get_search_attrs(sqs, request_data)
            sqs = sqs.autocomplete(title_auto=q[0])

        if q or 'page' in request_data:
            if 'page' in request_data:
                request_data.pop('page')


        # Sorting
        if 'sort_by' in request_data:
            sqs = sqs.order_by(request_data.pop('sort_by')[0])
        else:
            sqs = sqs.order_by(settings.DEFAULT_SORT_BY)

        gender = None
        if 'gender' in request_data:
            gender = request_data.pop('gender')[0]

        # By BRAND
        if 'attrs' in request_data:
            request_data.pop('attrs')
            for attr in request_data.keys():
                attrs = request_data.getlist(attr)
                if len(attrs) < 4:
                    sq = SQ()
                    for attr_val in attrs:
                        sq.add(SQ(attrs_exact__contains=(attr + ': ' + attr_val)), SQ.OR)
                    sqs = sqs.filter(sq)
                else:
                    return sqs

        if gender:
            sq = 'pol: Пол: Женский' if gender == 'female' else 'pol: Пол: Мужской'
            sqs = sqs.filter(attrs_exact__contains=sq)
            self.tree_categories = get_gender_cats(sqs)

        # Exclude non-canoncial products
        self.set_cats_and_attrs(sqs, request_data)
        sqs = sqs.exclude(structure="child")
        return sqs

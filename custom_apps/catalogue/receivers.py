from oscar.apps.catalogue.signals import product_viewed
from oscar.core.loading import get_classes

from django.dispatch import receiver


product_viewed = get_classes('catalogue.signals', ['product_viewed'])


@receiver(product_viewed)
def viewed_handler(sender, **kwargs):
    product = kwargs.get('product')
    if product:
        product.popularity += 1
        product.save()

from oscar.apps.offer import config


class OfferConfig(config.OfferConfig):
    name = 'custom_apps.offer'

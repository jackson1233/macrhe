from django import forms

from oscar.core.compat import get_user_model
from oscar.core.loading import get_model, get_class
from oscar.forms.mixins import PhoneNumberMixin

User = get_user_model()
Country = get_model('address', 'Country')
AbstractAddressForm = get_class('address.forms', 'AbstractAddressForm')


class ShippingAddressForm(PhoneNumberMixin, AbstractAddressForm):

    def __init__(self, *args, **kwargs):
        super(ShippingAddressForm, self).__init__(*args, **kwargs)
        self.adjust_country_field()

    def adjust_country_field(self):
        countries = Country._default_manager.filter(
            is_shipping_country=True)

        # No need to show country dropdown if there is only one option
        if len(countries) == 1:
            self.fields.pop('country', None)
            self.instance.country = countries[0]
        else:
            self.fields['country'].queryset = countries
            self.fields['country'].empty_label = None

    class Meta:
        model = get_model('order', 'shippingaddress')
        fields = [
            'first_name', 'last_name',
            'line4', 'line1', 'line2', 'line3', 'postcode',
            'phone_number', 'notes',
        ]
        labels = {
            'line1': 'Улица',
            'line2': 'Дом',
            'line3': 'Квартира'
        }


class PaymentMethodForm(forms.Form):
    methods = (
        ("NK", "Наличными курьеру"),
        ("NP", "Наложенным платежом"),
    )
    method_type = forms.ChoiceField(label="Способ оплаты", initial="Наличными курьеру", choices=methods)
    action = forms.CharField(widget=forms.HiddenInput, initial='place_order')

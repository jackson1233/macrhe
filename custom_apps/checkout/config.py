from oscar.apps.checkout import config


class CheckoutConfig(config.CheckoutConfig):
    name = 'custom_apps.checkout'

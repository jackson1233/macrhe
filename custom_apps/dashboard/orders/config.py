from oscar.apps.dashboard.orders import config


class OrdersDashboardConfig(config.OrdersDashboardConfig):
    name = 'custom_apps.dashboard.orders'

from django import forms

from oscar.core.loading import get_model, get_class
from treebeard.forms import movenodeform_factory
from oscar.core.utils import slugify

StockRecord = get_model('partner', 'StockRecord')
Category = get_model('catalogue', 'Category')
ProductAttribute = get_model('catalogue', 'ProductAttribute')
RelatedFieldWidgetWrapper = get_class('dashboard.widgets',
                                      'RelatedFieldWidgetWrapper')
ProductClass = get_model('catalogue', 'ProductClass')
CategoryForm = movenodeform_factory(
    Category,
    fields=['name', 'description'])


class StockRecordForm(forms.ModelForm):

    def __init__(self, product_class, user, *args, **kwargs):
        # The user kwarg is not used by stock StockRecordForm. We pass it
        # anyway in case one wishes to customise the partner queryset
        self.user = user
        super(StockRecordForm, self).__init__(*args, **kwargs)

        # Restrict accessible partners for non-staff users
        if not self.user.is_staff:
            self.fields['partner'].queryset = self.user.partners.all()

        # If not tracking stock, we hide the fields
        if not product_class.track_stock:
            for field_name in ['num_in_stock', 'low_stock_treshold']:
                if field_name in self.fields:
                    del self.fields[field_name]
        else:
            if 'num_in_stock' in self.fields:
                self.fields['num_in_stock'].required = True
                self.fields['num_in_stock'].min_value = 0
                self.fields['num_in_stock'].widget.attrs['min'] = 0
        if 'price_excl_tax' in self.fields:
            self.fields['price_excl_tax'].required = True
            self.fields['price_excl_tax'].min_value = 0.01
            self.fields['price_excl_tax'].widget.attrs['min'] = 0.01

    class Meta:
        model = StockRecord
        fields = [
            'partner', 'partner_sku',
            'price_currency', 'price_excl_tax', 'price_retail', 'cost_price',
            'num_in_stock', 'low_stock_threshold',
        ]


class ProductClassForm(forms.ModelForm):

    class Meta:
        model = ProductClass
        fields = ['name', 'requires_shipping', 'track_stock']


class ProductAttributesForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ProductAttributesForm, self).__init__(*args, **kwargs)

        # because we'll allow submission of the form with blank
        # codes so that we can generate them.
        self.fields["code"].required = False

        self.fields["option_group"].help_text = "Выберите группу опций"

        remote_field = self._meta.model._meta.get_field('option_group').remote_field
        self.fields["option_group"].widget = RelatedFieldWidgetWrapper(
            self.fields["option_group"].widget, remote_field)

    def clean_code(self):
        code = self.cleaned_data.get("code")
        title = self.cleaned_data.get("name")

        if not code and title:
            code = slugify(title)

        return code

    class Meta:
        model = ProductAttribute
        fields = ["name", "code", "type", "option_group", "required", "use_in_search"]

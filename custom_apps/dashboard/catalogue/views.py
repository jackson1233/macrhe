from django.http import HttpResponseRedirect

from oscar.apps.dashboard.catalogue.views import ProductCreateUpdateView as pcuw

from .signals import product_saved_totally


class ProductCreateUpdateView(pcuw):

    def forms_valid(self, form, formsets):
        """
        Save all changes and display a success url.
        When creating the first child product, this method also sets the new
        parent's structure accordingly.
        """
        if self.creating:
            self.handle_adding_child(self.parent)
        else:
            # a just created product was already saved in process_all_forms()
            self.object = form.save()

        # Save formsets
        for formset in formsets.values():
            formset.save()

        product_saved_totally.send(sender=self.__class__, instance=self.object)
        return HttpResponseRedirect(self.get_success_url())

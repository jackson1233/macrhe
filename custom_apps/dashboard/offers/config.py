from oscar.apps.dashboard.offers import config


class OffersDashboardConfig(config.OffersDashboardConfig):
    name = 'custom_apps.dashboard.offers'

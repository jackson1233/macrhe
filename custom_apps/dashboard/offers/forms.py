from django import forms

from oscar.core.loading import get_model

ConditionalOffer = get_model('offer', 'ConditionalOffer')


class MetaDataForm(forms.ModelForm):

    class Meta:
        model = ConditionalOffer
        fields = ('name', 'description', 'image')

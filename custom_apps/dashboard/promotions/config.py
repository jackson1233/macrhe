from oscar.apps.dashboard.promotions import config


class PromotionsDashboardConfig(config.PromotionsDashboardConfig):
    name = 'custom_apps.dashboard.promotions'

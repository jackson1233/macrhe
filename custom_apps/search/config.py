from oscar.apps.search import config


class SearchConfig(config.SearchConfig):
    name = 'custom_apps.search'

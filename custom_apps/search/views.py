from django.contrib import messages
from django.core.paginator import InvalidPage
from django.shortcuts import redirect
from django.views.generic import TemplateView

from oscar.core.loading import get_class, get_model


Product = get_model('catalogue', 'product')
Category = get_model('catalogue', 'category')
ProductAlert = get_model('customer', 'ProductAlert')
ProductAlertForm = get_class('customer.forms', 'ProductAlertForm')
get_product_search_handler_class = get_class(
    'catalogue.search_handlers', 'get_product_search_handler_class')


class SearchView(TemplateView):
    """
    Browse all products in the catalogue
    """
    context_object_name = "products"
    template_name = 'search/results.html'

    def get(self, request, *args, **kwargs):
        try:
            self.search_handler = self.get_search_handler(
                self.request.GET, request.get_full_path(), [])
        except InvalidPage:
            # Redirect to page one.
            messages.error(request, "Неправильный номер страницы")
            return redirect('catalogue:index')

        return super(SearchView, self).get(request, *args, **kwargs)

    def get_search_handler(self, *args, **kwargs):
        return get_product_search_handler_class()(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {}
        ctx['summary'] = "Все продукты"
        search_context = self.search_handler.get_search_context_data(
            self.context_object_name)
        ctx['tree_categories'] = self.search_handler.tree_categories
        ctx['attr_list'] = self.search_handler.attr_search
        suggestion = self.search_handler.search_form.get_suggestion()
        if suggestion != self.request.GET.get('q', ''):
            ctx['suggestion'] = suggestion
        ctx.update(search_context)
        return ctx

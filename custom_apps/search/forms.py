from collections import defaultdict

from django import forms
from django.conf import settings
from django.forms.widgets import Input
from haystack.forms import FacetedSearchForm

from oscar.core.loading import get_class

is_solr_supported = get_class('search.features', 'is_solr_supported')


class SearchInput(Input):
    """
    Defining a search type widget

    This is an HTML5 thing and works nicely with Safari, other browsers default
    back to using the default "text" type
    """
    input_type = 'search'


# Build a dict of valid queries
VALID_FACET_QUERIES = defaultdict(list)
for facet in settings.OSCAR_SEARCH_FACETS['queries'].values():
    field_name = "%s_exact" % facet['field']
    queries = [t[1] for t in facet['queries']]
    VALID_FACET_QUERIES[field_name].extend(queries)


class SearchForm(FacetedSearchForm):
    """
    In Haystack, the search form is used for interpreting
    and sub-filtering the SQS.
    """
    # Use a tabindex of 1 so that users can hit tab on any page and it will
    # focus on the search widget.
    q = forms.CharField(
        required=False, label="Поиск",
        widget=SearchInput({
            "placeholder": "Поиск",
            "tabindex": "1",
            "class": "form-control"
        }))

    # Search
    RELEVANCY = "relevancy"
    NEWEST = "newest"
    PRICE_HIGH_TO_LOW = "price-desc"
    PRICE_LOW_TO_HIGH = "price-asc"
    POPULARITY = "popularity-desc"

    SORT_BY_CHOICES = [
        (RELEVANCY, "Релевантные"),
        (POPULARITY, "Популярные"),
        (PRICE_HIGH_TO_LOW, "Сначала дорогие"),
        (PRICE_LOW_TO_HIGH, "Сначала дешевые"),
        (NEWEST, "Новые"),
    ]

    # Map query params to sorting fields.  Note relevancy isn't included here
    # as we assume results are returned in relevancy order in the absence of an
    # explicit sort field being passed to the search backend.
    SORT_BY_MAP = {
        NEWEST: '-date_created',
        PRICE_HIGH_TO_LOW: '-price',
        PRICE_LOW_TO_HIGH: 'price',
        POPULARITY: '-popularity',
    }

    sort_by = forms.ChoiceField(
        label="Сортировать", choices=SORT_BY_CHOICES,
        widget=forms.Select(), required=False)

    @property
    def selected_multi_facets(self):
        """
        Validate and return the selected facets
        """
        # Process selected facets into a dict(field->[*values]) to handle
        # multi-faceting
        selected_multi_facets = defaultdict(list)

        for facet_kv in self.selected_facets:
            if ":" not in facet_kv:
                continue
            field_name, value = facet_kv.split(':', 1)

            # Validate query facets as they as passed unescaped to Solr
            if field_name in VALID_FACET_QUERIES:
                if value not in VALID_FACET_QUERIES[field_name]:
                    # Invalid query value
                    continue

            selected_multi_facets[field_name].append(value)

        return selected_multi_facets

    def search(self):
        # We replace the 'search' method from FacetedSearchForm, so that we can
        # handle range queries
        # Note, we call super on a parent class as the default faceted view
        # escapes everything (which doesn't work for price range queries)
        return self.searchqueryset


class BrowseCategoryForm(SearchForm):
    """
    Variant of SearchForm that returns all products (instead of none) if no
    query is specified.
    """

    def search(self):
        # We replace the 'search' method from FacetedSearchForm, so that we can
        # handle range queries
        # Note, we call super on a parent class as the default faceted view
        # escapes everything (which doesn't work for price range queries)
        return self.searchqueryset

    def no_query_found(self):
        return self.searchqueryset

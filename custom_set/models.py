import uuid

from django.db import models
from django.urls import reverse

from oscar.core.compat import get_user_model

User = get_user_model()


class VerificationCode(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    code = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('confirm-email', kwargs={'ver_code': self.code})

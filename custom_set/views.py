import json

from django.http import HttpResponse
from django.urls import reverse
from django.conf import settings

from haystack.query import SearchQuerySet

from oscar.core.loading import get_class, get_model


Product = get_model('catalogue', 'product')

# get_product_search_handler_class = get_class(
    # 'catalogue.search_handlers', 'get_product_search_handler_class')


def get_prod_el(prod):
    return {'val': prod.title, 'url': reverse('catalogue:detail', kwargs={'product_slug': prod.slug, 'pk': int(prod.pk)}), 'class': prod.product_class}


def productAutocomplete(request):
    if request.is_ajax():
        sqs = SearchQuerySet().models(Product)
        if request.GET.get('q', ''):
            qs = sqs.autocomplete(title_auto=request.GET.get('q', ''))[:settings.RESULTS_PER_REQUEST]
        else:
            qs = sqs.all()[:settings.RESULTS_PER_REQUEST]
        result = [get_prod_el(prod) for prod in qs]
        return HttpResponse(json.dumps(result))
    else:
        return HttpResponse(status=404)

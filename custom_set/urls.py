from django.urls import path
from custom_apps.customer.views import confirm_email

urlpatterns = [
    path('confirm/<uuid:ver_code>', confirm_email, name="confirm-email")
]

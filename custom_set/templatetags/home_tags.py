
from oscar.core.loading import get_classes, get_class, get_model

from django import template

from haystack.query import SearchQuerySet


Product = get_model('catalogue', 'product')
Category = get_model('catalogue', 'Category')

register = template.Library()

EmailAuthenticationForm, EmailUserCreationForm = get_classes('customer.forms', ['EmailAuthenticationForm', 'EmailUserCreationForm'])
get_product_search_handler_class = get_class(
    'catalogue.search_handlers', 'get_product_search_handler_class')


def get_login_form(request):
    return EmailAuthenticationForm(
        **get_login_form_kwargs(request))


def get_login_form_kwargs(request):
    kwargs = {}
    kwargs['host'] = request.get_host()
    kwargs['prefix'] = 'login'
    kwargs['initial'] = {
        'redirect_url': request.GET.get('next', ''),
    }
    if request.method in ('POST', 'PUT'):
        kwargs.update({
            'data': request.POST,
            'files': request.FILES,
        })
    return kwargs


def get_registration_form(request):
    return EmailUserCreationForm(
        **get_registration_form_kwargs(request))


def get_registration_form_kwargs(request):
    kwargs = {}
    kwargs['host'] = request.get_host()
    kwargs['prefix'] = 'registration'
    kwargs['initial'] = {
        'redirect_url': request.GET.get('next', ''),
    }
    if request.method in ('POST', 'PUT'):
        kwargs.update({
            'data': request.POST,
            'files': request.FILES,
        })
    return kwargs


@register.simple_tag
def get_register_and_login_forms(request, login_form=None, registr_form=None):
    if type(login_form) == str:
        login_form = None
    if type(registr_form) == str:
        registr_form = None
    ctx = []
    a = get_login_form(request)
    a._errors = login_form
    ctx.append(a)
    b = get_registration_form(request)
    b._errors = registr_form
    ctx.append(b)
    if a._errors or b._errors:
        request.session['login_form'] = None
        request.session['registr_form'] = None
    return ctx


@register.simple_tag
def get_popular(request):
    qs = SearchQuerySet().order_by('-popularity')
    qs_pks = [int(item.pk) for item in qs[:5]]
    result = list(Product.objects.filter(pk__in=qs_pks).prefetch_related('images'))
    result.sort(key=lambda m: qs_pks.index(m.pk))
    return result


@register.simple_tag
def get_root_cats():
    return Category.get_root_nodes()

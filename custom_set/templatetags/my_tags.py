from django import template
from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils.translation import get_language, to_locale

from oscar.core.loading import get_model
from oscar.templatetags.category_tags import get_annotated_list

from custom_apps.basket.utils import get_attributes_of_prod

from decimal import Decimal as D
from decimal import InvalidOperation

from babel.numbers import format_currency

register = template.Library()

Category = get_model('catalogue', 'category')
Product = get_model('catalogue', 'product')


@register.filter(name='currency')
def currency(value, currency="RUB"):
    """
    Format decimal value as currency
    """
    try:
        value = D(value)
    except (TypeError, InvalidOperation):
        return u""
    # Using Babel's currency formatting
    # http://babel.pocoo.org/en/latest/api/numbers.html#babel.numbers.format_currency
    OSCAR_CURRENCY_FORMAT = getattr(settings, 'OSCAR_CURRENCY_FORMAT', None)
    kwargs = {
        'currency': "RUB",
        'locale': to_locale(get_language() or settings.LANGUAGE_CODE)
    }
    if isinstance(OSCAR_CURRENCY_FORMAT, dict):
        kwargs.update(OSCAR_CURRENCY_FORMAT.get(currency, {}))
    else:
        kwargs['format'] = OSCAR_CURRENCY_FORMAT
    return format_currency(value, **kwargs)


@register.simple_tag
def get_attributes_of_line(product):
    return mark_safe(get_attributes_of_prod(product))


@register.simple_tag
def product_count(category):
    '''
    return product count of category
    '''

    if not category:
        return '0'

    count = 0
    categories = category.get_descendants_and_self()
    for category in categories:
        count += category.product_set.count()
    return count


@register.simple_tag
def purchase_info_for_prod(request, product):
    res = {}
    strats = []
    min_max = None
    if product.is_parent:
        # Fetch all "Purchase info's" of product variant's
        prods = product.children.all()
        prсs_list = []

        for x in prods:
            strat = request.strategy.fetch_for_product(x)
            prсs_list.append(strat.price.incl_tax)
            strats.append(strat)
        min_max = str(min(prсs_list)).replace('.', ',') + ' - '
        min_max += str(max(prсs_list)).replace('.', ',')
        min_max += settings.CUSTOM_CURRENCY_FORMAT

    else:
        strats.append(request.strategy.fetch_for_product(product))
    res['session'] = strats
    res['min_max'] = min_max
    return res


@register.simple_tag
def get_sort_tags(request):
    sort_by = {}
    sort_by_curr = request.GET.get('sort_by', settings.DEFAULT_SORT_BY)

    for sort_key, sort_lbl in settings.SEARCH_SORT_BY.items():
        if sort_by_curr == sort_key:
            sort_by.setdefault('-' + sort_key, sort_lbl)
        else:
            sort_by.setdefault(sort_key, sort_lbl)

    return [sort_by, sort_by_curr]

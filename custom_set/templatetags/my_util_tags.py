from django import template
from custom_apps.customer import history
from custom_apps.partner.utils import rubtousd
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter(is_safe=False)
@stringfilter
def rupluralize(value, forms):
    """
    Подбирает окончание существительному после числа
    {{someval|pluralize:"товар,товара,товаров"}}
    """
    try:
        one, two, many = forms.split(u',')
        value = str(value)[-2:]  # 314 -> 14

        if (21 > int(value) > 4):
            return many

        if value.endswith('1'):
            return one
        elif value.endswith(('2', '3', '4')):
            return two
        else:
            return many

    except (ValueError, TypeError):
        return ''


@register.inclusion_tag('customer/history/recently_viewed_products.html',
                        takes_context=True)
def recently_viewed_products(context, current_product=None):
    """
    Inclusion tag listing the most recently viewed products
    """
    request = context['request']
    products = history.get(request)
    if current_product:
        products = [p for p in products if p != current_product]
    return {'products': products,
            'request': request}


@register.simple_tag
def debug_cool_tool(thing):
    print('Type of Thing: ', type(thing), '\n\n\n')
    print('Dir\'s of thing: ', dir(thing), '\n\n\n')
    print('Thing: ', thing, '\n\n\n')


@register.filter(name='zip')
def zip_lists(a, b):
    return zip(a, b)


@register.simple_tag
def get_currencie():
    return rubtousd()

from oscar.core.loading import get_model

from haystack import signals
from haystack.exceptions import NotHandled

from custom_apps.dashboard.catalogue.signals import product_saved_totally

Product = get_model('catalogue', 'product')


class ProductSignalProcessor(signals.BaseSignalProcessor):
    def setup(self):
        product_saved_totally.connect(self.handle_save)

    def teardown(self):
        product_saved_totally.disconnect(self.handle_save)

    def handle_save(self, sender, instance, **kwargs):
        """
        Given an individual model instance, determine which backends the
        update should be sent to & update the object on those backends.
        """
        using_backends = self.connection_router.for_write(instance=instance)
        for using in using_backends:
            try:
                index = self.connections[using].get_unified_index().get_index(Product)
                index.update_object(instance, using=using)
            except NotHandled:
                # TODO: Maybe log it or let the exception bubble?
                pass
        if instance.is_child:
            instance.parent.save()

from django.apps import AppConfig


class CustomSetConfig(AppConfig):
    name = 'custom_set'

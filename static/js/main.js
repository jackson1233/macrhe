// Product init-func
function initProd() {
    if ($('.prd-opt-link').length <= 1) {
        $('#id_child_id').prop("selectedIndex", 0);
        $('.prd-opt-link').first().addClass('opt-selected');
        $('.btn-add-to-basket').addClass('active-btn');
    }
}

// Helper functions
function changeBask(change_numb) {
    $.ajax({
            type: "POST",
            url: "/basket/",
            beforeSend: function() { 
                $('#basket-totals-wrapper').addClass('loading');
                $('.cnt-bask').addClass('disabled');
            },
            complete: function() { 
                $('#basket-totals-wrapper').removeClass('loading');
                $('.cnt-bask').removeClass('disabled');
            },
            data: $("#basket_formset").serialize(),
                
           }).done(function(msg) {
                var item_len =  $('.basket-items').length;
                $('#basket_totals').empty();
                if (item_len <= 0) {
                    $('.basket-wrapper').remove();
                    $('.nav-1-total').html('Корзина');
                } else {
                    $('#basket_totals').html(msg.content_html);
                    $('.basket-num').html(parseInt($('.basket-num').html()) + change_numb);
                    ajax_block = $('.ajax-messages');
                    ajax_block.empty()
                    if (msg.messages.warning) {
                        msg.messages.warning.forEach(function(i) {
                            ajax_block.append('<div class="alert alert-warning">'+i+'</div>')
                        })
                    }
                    if (msg.messages.success) {
                        $('.upsell_messages').remove();
                        msg.messages.success.forEach(function(i) {
                            ajax_block.append('<div class="alert alert-success">'+i+'</div>')
                        })
                    }
                }
           }).fail(function(msg) {
                console.log(msg);
           });
}

function refreshBask() {
    var forms_num = parseInt($('#id_form-TOTAL_FORMS').attr('value'))
    $('#id_form-TOTAL_FORMS').attr('value', forms_num - 1);
    $('#id_form-INITIAL_FORMS').attr('value', forms_num - 1);
    $('.basket-items').each(function(i) {
        var item_cont = $(this).children();
        var item_id = item_cont.first();
        var item_id_num = parseInt(item_id.attr('name').replace('form-', '').replace('-id', ''));
        var item_quan = item_cont.find('.quantity_inp');
        var item_cnts = item_quan.next().children();
        var item_cnt_min = item_cnts.first();
        var item_cnt = item_cnt_min.next();
        var item_cnt_plus = item_cnt.next();

        if (i != item_id_num) {
            $(this).attr('id', 'id_form-'+String(i)+'-quantity-id')
            item_id.attr('name', 'form-'+String(i)+'-id');
            item_id.attr('id', 'id_form-'+String(i)+'-id');
            item_quan.attr('name', 'form-'+String(i)+'-quantity');
            item_quan.attr('id', 'id_form-'+String(i)+'-quantity');
            item_cnt_min.attr('quan-id', 'id_form-'+String(i)+'-quantity');
            item_cnt.attr('id', 'id_form-'+String(i)+'-quantity-cnt')
            item_cnt_plus.attr('quan-id', 'id_form-'+String(i)+'-quantity');
        }
    });
}

function doBounce(element, times, distance, speed, remove_query) {
    element.clearQueue()
           .stop()
           .css({
            marginTop: 0
           });
    for(i = 0; i < times; i++) {

        if ((i + 1) == times) {
            element.animate({marginTop: '-='+distance},speed)
                   .animate({marginTop: '+='+distance},speed, function() {
                        element.removeClass('error-cnt');
                        $(remove_query).removeClass('disabled');
                    });
        } else {
            element.animate({marginTop: '-='+distance},speed)
                   .animate({marginTop: '+='+distance},speed);
        }
    }
}

function dropdownFC(element) {
    if (element == 0) {
        $('.prd-sort').removeClass('open');
        $('.side-categories').toggleClass('open');
        $("#dropdownFilters").removeClass('active');
        $("#dropdownCategories").toggleClass('active');
        $("#dropdownCategories").removeClass('notactive');   
        $("#dropdownFilters").toggleClass('notactive');   
    } else {   
        $('.side-categories').removeClass('open');
        $('.prd-sort').toggleClass('open');
        $("#dropdownCategories").removeClass('active');
        $("#dropdownFilters").toggleClass('active');
        $("#dropdownFilters").removeClass('notactive'); 
        $("#dropdownCategories").toggleClass('notactive'); 
    }
}

if(window.innerWidth < 1000) {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('.dragscroll').css('overflow', 'auto');
    }
}

$('#modal-swap').click(function(e) {
    // Use class in 'using' to exclude clicking at animation time
    e.target.classList.add('using');

    // Swap text in modal
    authLbl = $('#authModaLLabel');
    tmp = authLbl.html();

    // Change form
    form_login = $('#login_form');
    register_form = $('#register_form');
    if (form_login.is(":visible")) {
        form_login.fadeOut(function() {
            register_form.fadeIn("fast");
            authLbl.html(e.target.innerHTML);
            e.target.innerHTML = tmp;
            e.target.classList.remove('using');
        });
    } else {
        register_form.fadeOut(function() {
            form_login.fadeIn("fast");
            authLbl.html(e.target.innerHTML);
            e.target.innerHTML = tmp;
            e.target.classList.remove('using');
        });
    }
});

$("#search_btn_mob").click(function(e) {
    $('#search_btn_mob').removeClass('visible-xs');
    $('#search_form').removeClass('hidden-xs');
    $('#search_form').addClass('open-search');
    $('body').css('overflow', 'hidden');
    $('.tt-menu').addClass('tt-open');

    if (!$('.close-search').length) {
        var el = document.createElement('a');
        el.className = 'close-btn';
        el.href = "#";
        $('.twitter-typeahead').append(el);
        el.onclick = close_search;
        $(el).append('<span class="close-search"></span>')
    }
});

function close_search() {
    $('#search_form').addClass('hidden-xs');
    $('#search_form').removeClass('open-search');
    $('body').css('overflow', '');
    $('.tt-menu').removeClass('tt-open');
    $('#search_btn_mob').addClass('visible-xs');
}

$(".prd-opt-link").click(function(e) {
    e.preventDefault();
    element = $(this);
    $('.prd-opt-link').removeClass('opt-selected');
    element.addClass('opt-selected');
    $('#id_child_id').val(element.attr('opt'));
    if (!$('.active-btn').length) {
        $('.btn-add-to-basket').addClass('active-btn');
    }
});


function isScrollable(container) {
    var isScrlL = $(container).scrollLeft();
    var isScrlR = ($(container)[0].scrollWidth - isScrlL) - $(container)[0].clientWidth;
    if (!isScrlR) {
        $(container).next().addClass('disabled');
    } else {
        $(container).next().removeClass('disabled');
    }
    if (!isScrlL) {
        $(container).prev().addClass('disabled');
    } else {
        $(container).prev().removeClass('disabled');
    }
}

function scroll_div(direction, container) {
    var direct;
    if (direction == 0) {
        direct = '-';
    } else if (direction == 1) {
        direct = '+';
    }

    if (direct) {
        $(container).animate({scrollLeft: direct + '=' + $(container)[0].clientWidth + 'px'}, 500, function () {
            isScrollable(container); 
        });
    }
}

function _isDropOrSubmit() {
    $("#subm_drop_btn").html("Сбросить фильтры<span></span>");
    $("#subm_drop_btn").addClass("drop-btn");
}

function isDropOrSubmit() {
    var selectedLen = 0;
    for (var i = 0; i < $multiSel.length; i++) {
        selectedLen += $($multiSel[i]).multipleSelect('getSelects').length;
        if (selectedLen > 0) {
            _isDropOrSubmit();
            return;
        }
    }
    $("#subm_drop_btn").html("Применить фильтры");
    $("#subm_drop_btn").removeClass("drop-btn");
}

$("#subm_drop_btn").click(function(e) {
    var drop_btn = $("#subm_drop_btn");
    if (drop_btn.hasClass("drop-btn")) {
        e.preventDefault();
        $multiSel.multipleSelect('uncheckAll');
        $("#subm_drop_btn").html("Применить фильтры");
        $("#subm_drop_btn").removeClass("drop-btn");
    }
});

$('.sort-i').click(function(e) {
    if(window.innerWidth < 1000) {
        item = $(e.target);
        item_key = item.attr('sort-by');
        item_opt = $("#"+item_key);
        item_val = item_opt.val();
        items = $('.sort-i');
        if (item_val[0] == "-") {
            item_opt.val(item_val.slice(1));
            items.removeClass('desc');
            items.removeClass('asc');
            items.removeClass('sort-active');
            item.addClass('sort-active');
            item.addClass('asc');
        } else {
            item_opt.val('-' + item_val);
            items.removeClass('asc');
            items.removeClass('desc');
            items.removeClass('sort-active');
            item.addClass('sort-active');
            item.addClass('desc');
        }
        $("#sort_by_select").val(item_opt.val());
        $("#subm_drop_btn").html("Применить фильтры");
        $("#subm_drop_btn").removeClass("drop-btn");
    } else {
        item = $(e.target);
        window.location.search = $.query.set('sort_by', item.attr('sort-by'));
    }    
});
$('.cat-link').click(function(e) {
    e.preventDefault();
    window.location.href = $(e.currentTarget).attr('href') + window.location.search;
});
$('.sort-lbl').click(function(e) {
    $('.sort-cont').slideToggle(function() {
        $("#sort_list").toggleClass('open');
    });
});

var rub_curr = parseFloat($("#currencie_tag").attr('currencie').replace(',', '.'));

$('.price_in_usd input').change(function(e) {
	if (e.originalEvent) {
		var price_in_usd = parseFloat(e.target.value);
		var price_rub_el = $(e.target).parent().parent().parent().next().children();
		price_rub_el.val(parseInt(price_in_usd * rub_curr));
	}
});
$('.price_in_rub input').change(function(e) {
	if (e.originalEvent) {
		var price_in_rub = parseFloat(e.target.value);
		var price_usd_el = $(e.target).parent().prev().find('input');
		price_usd_el.val((price_in_rub / rub_curr).toFixed(2));
	}
});
$('.price_in_rub input').each(function(i) {
	var el = $($('.price_in_rub input')[i]);
	if (el.length) {
		var price_in_usd = el.parent().prev().find('input').val();
		el.val(parseInt(rub_curr * price_in_usd));
	}
});